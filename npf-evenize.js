/*
	NPF IMAGES READJUSTMENT | @glenthemes
	v2.0 [last updated: 2022-06-26] 12:31PM GMT-7
	
	Latest updates: [mostly for my own record]
  > automatically add tumblr's in-built npf css if
    the blog doesn't have it
	> added click-slideshow for each photoset
	  (click to go to next image, like on dashboard)
	  (currently does not support arrow keys)
	> improved smart-grouping of multi-row image sets
	> added .photo_origin class to posts that are meant to be a photo-typed post
	> added .nai class to npf photo[s] that are NOT in the first relog-head
	> fixed issue with scrollbar disappearing on npf videos
	> fixed issue where opening the last image in a set,
	  then clicking its lightbox image would make it adopt 'next image' behavior
	> added relocate fallback for old captions
*/

$(document).ready(function(){
  
    // append npf-build.css
    if(!$("link[href*='blog-network-npf/index.build.css']").length){
        $("head").prepend("<link rel='stylesheet' media='screen' href='//assets.tumblr.com/client/prod/standalone/blog-network-npf/index.build.css'>");
    }
  
    // check for icons-sized images
    // if true, don't stretch
    $(".tmblr-full [data-big-photo-width]").each(function(){
        let dbpw = Number($(this).attr("data-big-photo-width"));
        if(dbpw < 300){
            $(this).addClass("npf-small")
        }
    })
    
    var enculer = getComputedStyle(document.documentElement)
                  .getPropertyValue("--NPF-Bottom-Gap-From-Captions");
                  
    // wrap each row
    $(".npf_row").each(function(){
        $(this).wrap("<div class='npf_row_set'>");
        
    });
    
    // make each row (and its items) even
    $(".tmblr-full").each(function(){
        if($(this).closest(".npf_col").length){
            $(this).unwrap()
        }
		
		if($(this).find("video").length){
			$(this).addClass("npf-video");
		}
        
        var imgheight = $(this).height();
        if(imgheight < $(this).parents(".npf_row").height()){
            $(this).parents(".npf_row").height(imgheight)
        }
        
        if(!$(this).closest(".npf_row").length){
            $(this).wrap("<div class='npf_row_parent'>");
        };
    });
    
    $(".npf_row_parent").each(function(){
        if($(this).siblings(".npf_row_parent").length){
            $(this).addClass("bROTHER");
        }
        
        if(!$(this).closest(".npf_row_set").length){
            $(this).wrap("<div class='npf_row_set'>");
        }
        
        if(!$(this).siblings().length){
            if($(this).closest(".npf_row_set").length){
                $(this).unwrap();
            }
        }
        
        /*
        if(!$(this).hasClass("bROTHER")){
            $(this).wrap("<div class='npf_inst'>");
        }
        */
    });
    
    
    $(".npf_row_parent, .npf_row_set").each(function(){
        if($(this).next().is("[class^='npf_row_']")){
            $(this).add($(this).next()).wrapAll("<div class='npf_temp'></div>")
        }
    });
    
    $(".npf_temp").each(function(){
        if($(this).next().is("[class^='npf_row_']")){
            $(this).next().appendTo($(this))
        }
        
        $(this).addClass("npf_inst").removeClass("npf_temp")
    })
    
    // check jquery version
    var jqver = jQuery.fn.jquery;
    var ver = jqver.replaceAll(".","");
    
    // wrapping script courtesy of Nick Craver, my god and savior
    // stackoverflow.com/a/3329249/8144506
    
    if(ver < "180"){
        var divs = $(".bROTHER");
        for(var i=0; i<divs.length;) {
           i += divs.eq(i).nextUntil(':not(.bROTHER)').andSelf().wrapAll('<div class="npf_inst">').length;
        }
    } else {
        var divs = $(".bROTHER");
        for(var i=0; i<divs.length;) {
           i += divs.eq(i).nextUntil(':not(.bROTHER)').addBack().wrapAll('<div class="npf_inst">').length;
        }
    }
    
    // if npf only has 1 row by itself, wrap self as inst
    $(".npf_row_set, .npf_row_parent").each(function(){
        if(!$(this).closest(".npf_inst").length){
            if(!$(this).siblings("[class^='npf_row_']").length){
                $(this).wrap("<div class='npf_inst'></div>")
            }
        };
    });
	
    
	/*
    // init lightbox per image click
	// i blotted out this code bc it wasn't rly necessary
    $(".tmblr-full").click(function(){		
        var img = $(this).find("img"),
            imgsrc = img.attr("src");
        
        Tumblr.Lightbox.init([{
            low_res:imgsrc,
            high_res:imgsrc
        }]);
        
        $("body").toggleClass("tumblr_lightbox_active");
        return false;
    });
    */
	
	// ok maybe it is necessary
	$(".npf_row_parent .tmblr-full:not(.npf-video)").click(function(){		
        var img = $(this).find("img"),
            imgsrc = img.attr("src");
        
        Tumblr.Lightbox.init([{
            low_res:imgsrc,
            high_res:imgsrc
        }]);
    });
	
    // wrap npf sets in a container
    $("blockquote").each(function(){
        $(this).children(".npf_row_set").wrapAll("<div class='npf_inst'>");
    });
    
    // target npf text posts only
    $("*").filter('[post-type="text"]').addClass('textpost');
    
    // wrap npf in container even if it isn't inside a blockquote
    $(".textpost").each(function(){
        if($(this).find("blockquote").next(".npf_row_set").length){
            $(this).find("blockquote").siblings(".npf_row_set").wrapAll('<div class="npf_inst">')
        }
    });
	
	// turn npf images HD
    $(".npf_inst img").each(function(){
        var getHD = $(this).parent(".post_media_photo_anchor").attr("data-big-photo");
        $(this).attr("src",getHD);
        
        var newsrc = $(this).attr("src").replace("_" + 500 + ".", "_" + 1280 + ".");
        $(this).attr("src",newsrc);
    });
    
    //remove empty <br> generated by mobile uploads
    $(".npf_inst").each(function(){
        var checkpara = $(this).next("p");
        
        checkpara.contents().filter(function(){
            return this.nodeType === 3;
        }).wrap('<span/>');
        
        if(checkpara.length){
            if(checkpara.children("br").length){
                if(!checkpara.children("br").eq(0).siblings().length){
                    checkpara.remove();
                }
            }
        }
    });
    
    // find & remove empty <p>s generated before npf photosets
    // do nothing if no empty <p>s have been generated
    $(".textpost p").each(function(){
        if($(this).text().length == 0){
            if($(this).children().length == 0){
                if($(this).prev().length == 0){
                    if($(this).next(".npf_inst")){
                        $(this).remove()
                    }
                }
            }
        }
    });
    
    $(".textpost").each(function(){
        if($(this).find(".npf_inst").length){
            $(this).addClass("has-npf");
        }
    });
    
    // make the row height the same as shortest image in the row
    function yes(){
        $(".tmblr-full img").each(function(){
            $(this).attr("genheight",$(this).height());
        });
        
        $(".npf_row_set").each(function(){
            var que = $(this).find(".tmblr-full img").map(function(){
                return $(this).attr("genheight");
            }).get();
        
            var shortest = Math.min.apply(Math,que);
            $(this).find(".npf_row").height(shortest);
            
            $(this).find(".tmblr-full").height(shortest)
        });
    }
    
    yes();
    
    
    $(".textpost p").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType === 3;
        }).wrap('<span/>');
    });
    
    // if npf post only has 1 instance of npf_inst
	$(".textpost.has-npf").each(function(){
		var npf_inst = $(this).find(".npf_inst");
		if(npf_inst.length === 1){
			/*
			if(npf_inst.next().is(".comment_container")){
				npf_inst.css("margin-bottom","1em");
			}
			*/
			
			if(npf_inst.prev().length == 0){
				npf_inst.addClass("boooob");
			}
			
			if(npf_inst.siblings().length == 0){
				npf_inst.removeClass("boooob");
			}
		}
	});
    
    /*------------ RELOCATE NPF TO TOP OF POST ------------*/
    
    // make npf photos look like a photo post
    // move npf photoset above the @username and caption
            
    // only relocate if there is 1 line of caption
    // and that it's meant to be after the photo
    
    /*------------ LEGACY ------------*/
    $(".has-npf").each(function(){
        if($(this).find(".npf_inst").length === 1){
            var npf = $(this).find(".npf_inst");
            
            if(!npf.prev().length){
                if(npf.closest("blockquote").length){
                    if(npf.parent().prev("p")){
                        if(npf.parent().prev().find("a.tumblr_blog").length){
                            
                            npf.insertBefore(npf.parent().prev());
                        }
                    }
                }
            }
        }
    });//has-npf each
	
	// remove empty blockquotes hanging at the end (take2)
	function unblockquote(){
    $(".textpost.has-npf blockquote").each(function(){
        if(!$.trim($(this).html()).length){
            $(this).remove();
        }
    });
	}
	
	unblockquote();
    
    // doublecheck if any photosets are still stuck inside a blockquote
	function _unwrap(){
    $(".textpost.has-npf").each(function(){
        if($(this).find(".npf_inst").length === 1){
            var npf = $(this).find(".npf_inst");
            if(!npf.prev().length){
                if(npf.closest("blockquote").length){
                    if(npf.parent().prev("p")){
                        if(npf.parent().prev().find("a.tumblr_blog").length){
                            npf.insertBefore(npf.parent().prev())
                        }
                    }
                }
            }
        }
    });
	}
	
	_unwrap();
    
    /*------------ MODERN ------------*/
    $(".textpost .npf_inst").each(function(){
        
        if($(this).hasClass("photo-origin")){
        if($(this).next("p").find("a.tumblr_blog").length == 0){
            if(!$(this).prev().length){
                if(!$(this).parent().is("blockquote")){
                    if($(this).next().length){
                        if(!$(this).parent().parent().is(".textpost")){
                        if(!$(this).parent().is(".textpost")){
                            $(this).insertBefore($(this).parent().parent());
                            $(this).css("margin-bottom",enculer);
                        }
                        }
                        
                    } else {
                        $(this).css("margin-top",enculer)
                    }
                }
                
            }
        }
        }
    });
    
	
	// group npf images if they're ungrouped
	if(ver < "180"){
        var ins = $(".npf_inst");
        for(var k=0; k<ins.length;) {
           k += ins.eq(k).nextUntil(':not(.npf_inst)').andSelf().wrapAll('<div class="npf_group">').length;
        }
    } else {
        var ins = $(".npf_inst");
        for(var k=0; k<ins.length;) {
           k += ins.eq(k).nextUntil(':not(.npf_inst)').addBack().wrapAll('<div class="npf_group">').length;
        }
    }
	
	$(".npf_group").each(function(){
		$(this).children(".npf_inst").children().unwrap();
		$(this).addClass("npf_inst").removeClass("npf_group");
	});
	
	// check if there are multiple .npf_inst's stuck together
	// if yes and there's an extra gap, minimize that gap
	$(".npf_inst").each(function(){
		if($(this).next().is(".npf_inst")){
			$(this).css("margin-bottom","var(--NPF-Image-Spacing)")
		}
	});
	
	_unwrap();
	
	unblockquote();
	
	
	// add post source
	$("a.tumblr_blog").each(function(){
		// $(this).addClass("fucktumblr");
		
		// check if this post doesn't have any reblogs/captions
		if($(this).parents(".textpost").find("blockquote").length == 0){
			
			// only add post-src if post doesn't have src
			if($(this).next("span").length){
				$(this).next().remove();
				
				if($(this).parent().is("p")){
					if($(this).parent().prev().is(".npf_inst")){
						var opurl = $(this).attr("href");
						var optxt = $(this).text();
						$(this).parent().addClass("post-src").html("(Source: <a href='" + opurl + "'>" + optxt + "</a>)")
					}
				}
			}
		}
	});
	
	// relocate post source to bottom of all reblogs
    $(".post-src").each(function(){
        if($(this).next().is("blockquote") || $(this).next().is("p")){
             $(this).parent().append($(this));
        }
    });
	
	/*------ MODERN ONLY ------*/
	// if post hasn't been relocated [i.e. no captions or reblogs],
	// add space between photoset and reblogger name
	$(".npf_inst").each(function(){
		if($(this).siblings().length == 0){			
			if(!$(this).parent().is("blockquote")){
				$(this).css("margin-top",enculer)
			}
		}
	});
	
	// if the thing below npf already has a gap,
	// don't apply an additional gap
	$(".npf_inst").each(function(){
		var nextThing = $(this).next();
		var getmarge = parseFloat(nextThing.css("margin-top"));
		var getpad = parseFloat(nextThing.css("padding-top"));
		
		nextThing.attr("nxtmargin",getmarge);
		nextThing.attr("nxtpad",getpad);
		
		if(nextThing.attr("nxtmargin") > 0 || nextThing.attr("nxtpad") > 0){
			$(this).css("margin-bottom","")
		}
		
		if(nextThing.attr("nxtmargin") == 0 && nextThing.attr("nxtpad") == 0){
			$(this).css("margin-bottom",enculer)
		}
	});
	
	
    // fallback for if npf heights are returning zero
    var started = Date.now();
		
	var interval = setInterval(function(){
		if(Date.now() - started > 5000){
			clearInterval(interval)
		} else {
			$(".npf_row").each(function(){
		        u = $(this).height();
        		  if (u == "0") {
			            yes();
 		         }
			});
		}
	},0); // every 0 seconds
	
	$(".npf_inst").each(function(){
		if($(this).parents(".npf_inst").length){
		    if($(this).siblings("[class^='npf_']").length == 0){
		        $(this).children().unwrap();
		    }
		}
	});
	
	_unwrap();
	
	/*-------- LIGHTBOX: move to next image if available --------*/
    // assign unique ID to each NPF photoset
    $(".npf_inst").each(function(){
        $(this).attr("npf-id","npf_" + Math.random().toString(36).substr(2, 5))
    });
    
    // initialize number of images in each NPF photoset,
    // and create an numerically labelled list
    $(".npf_inst").each(function(){
        $(this).find(".tmblr-full").each(function(i){
            i = i + 1;
            $(this).attr("ordre",i);
        });
        
        $(this).find(".tmblr-full img").each(function(w){
            w = w + 1;
            $(this).parents(".npf_inst").attr("image" + w,$(this).attr("src"))
        });
    })
    
    // when user first clicks an NPF image from its set
    $(".tmblr-full img").click(function(){
        var that = this;
        
        setTimeout(nice,69);
        
        function nice(){
            
            $(".lightbox-image").attr("npf-id",$(that).parents("[npf-id]").attr("npf-id"));
            
            $(".lightbox-image").click(function(){
                
                var currentIMG = $(".lightbox-image").attr("src");
            
                // position in lineup
                if($(that).attr("src") == currentIMG){
                    var pos = $(that).parents(".tmblr-full").attr("ordre");
                }
                
                var nextN = Number(pos.toString()) + Number(1);
                
                // check if there is a next image
                if($(that).parents(".npf_inst").is("[image" + nextN + "]")){
                    
                    
                    $(".lightbox-image-container").removeClass("single-image");
					if($(that).parents(".npf_inst").is("[image" + nextN + "]")){
                    	$(this).addClass("npf_hasnext");
						$(".lightbox-image-container").css({
                        	"top":"50%",
                        	"left":"50%",
                        	"transform":"translate(-50%,-50%)"
                    	});
					}
                    
                    var getnextimg = $(that).parents(".npf_inst").attr("image" + nextN);
                    
                    $(this).attr("src",getnextimg);
                    
                    if($(this).height() > $(this).width()){
                        $(this).removeAttr("width");
                    } else {
                        $(this).removeAttr("height");
                    }
                    
                }
                
            });
        }
    })
    
    // when user clicks through the images (in LIGHTBOX mode)
    $(document).on("click", ".npf_hasnext", function(){
        var that = this;
        
        var currentIMG = $(".lightbox-image").attr("src");
        var npftarget = $(that).attr("npf-id");
        
        $(".npf_inst").each(function(){
            if($(this).attr("npf-id") == npftarget){
                var IMGs = this.attributes;
                $.each(IMGs,function(i,img){
                    if(img.value == currentIMG){
                        var pick = img.name;
                        var nextnum = Number(pick.replace(/[^\d-]/g,"")) + Number(1);
                        $(that).attr("next",nextnum)
                    }
                });
                
                var getnxt = $(this).attr("image" + $(that).attr("next"));
                $(that).attr("src",getnxt)
            }
        })
    })
    
    $(".npf_inst").each(function(){
        if($(this).prev().length == 0){
            $(this).addClass("boooob")
        }
    })
    
    // check which posts are meant to look like a photo post
    // aka 1 NPF photo[set] only
    // ONLY in the first reblog, not subsequent
    $(".textpost.has-npf").each(function(){
        var that = this;
        var target_npf = $(that).find(".npf_inst").eq(0);
        if($(this).find(".npf_inst").length == 1){
            var kae = Date.now();
            var sped = setInterval(function(){
                if(Date.now() - kae > 5000){
                    $(".forceint").removeClass("forceint");
                    clearInterval(sped)
                } else {
                    
                    
                    // check if the soloNPF is in the FIRST reblog or not
                    var ph = $(that).children().first().attr("class");
                    if($(that).children().first().siblings("." + ph).length == 0){
                        target_npf.addClass("photo_origin")
                    } else {
						// if there is more than one reblogger
						if($(that).children("." + ph).first().find(".npf_inst").length == 0){
							target_npf.removeClass("photo_origin").addClass("nai")
						}
					}
					
					// get npf's grandparent class name
					// then see if there is a thing before it w/ that name
					var gpn = target_npf.parent().parent().attr("class");
					var gpn_b = target_npf.parent().parent().prev().attr("class");
					
					if(gpn.indexOf(gpn_b) || gpn_b.indexOf(gpn) || gpn == gpn_b){
					    target_npf.removeClass("photo_origin")
					}
                    
                    // find the single npf and target its parent
                    // npf's parent has class .forceint
                    // $(this) == .forceint [npf's direct parent]
                    target_npf.parent().addClass('forceint');
                    $(".forceint").each(function(){
                        if($(this).children().first().is(".npf_inst")){
                            if($(this).attr("post-type") == "text"){
                                $(this).children().first(".npf_inst").addClass("photo_origin")
                            }
                        }
                    })
                    
                    if(target_npf.prev().is("a[href]")){
                        if(target_npf.parent().is("blockquote[class]")){
                            target_npf.addClass("photo_origin")
                        }
                    }
                    
                    if(!target_npf.prev().length){
                        if(!target_npf.parent().prev().length){
                            target_npf.addClass("photo_origin")
                        }
                    }
                    
                    if(target_npf.parent().prev().is("a[href]")){
                        if(!target_npf.parent().is(".textpost")){
                            target_npf.addClass("photo_origin")
                        }
                    }
                    
                    // add .nai class to NON first-reblog npf
                    $(".nai").each(function(){
                        if($(this).hasClass("photo_origin")){
                            $(this).removeClass("photo_origin boooob")
                        }
                    })
                    
                    // increase space between NON first-reblog npf
                    // and its caption underneath
                    $(".npf_inst").each(function(){
                        if(!$(this).prev().length){
                            if($(this).next().length){
                                if(!$(this).next().is("div")){
                                    if(!$(this).hasClass("photo_origin")){
                                        if(parseFloat($(this).parent().css("margin-bottom")) > 0 || parseFloat($(this).parent().prev().css("margin-bottom")) > 0){
                                            // nothing
                                        } else {
                                            $(this).css("margin-top",enculer)
                                        }

                                    }
                                }
                            }
                        }
                    })
                    
                    tln();
                }
            },0);
        }
    })
    
    // relocate again - MODERN ONLY
    function tln(){
        $(".photo_origin").each(function(){
            if($(this).prev().is("a[href]")){
                if($(this).parent().is("blockquote")){
                    $(this).prependTo($(this).parent())
                }
            }
            
            if($(this).parent().prev().is("a[href]")){
                if(!$(this).parent().parent().is("blockquote")){
					if($(this).next().length){
                    	$(this).prependTo($(this).parent().parent());
						$(this).css("margin-bottom",enculer)
					} else {
						$(this).removeClass("photo_origin boooob");
						$(this).css("margin-top",enculer)
					}
                }
            }
        })
    }
	
	// apply .photo_origin class again if it didn't
	var gae = Date.now();
	var oppai = setInterval(function(){
        if(Date.now() - gae > 5000){                    
        	clearInterval(oppai)
        } else {
			$(".boooob").each(function(){
				if(!$(this).prev().length){
					$(this).addClass("photo_origin");
					
					// only allow ONE instance of .photo_origin to exist
					$(".photo_origin").each(function(){
						if($(this).parent().is(".photo_origin")){
							$(this).removeClass("photo_origin");
						}
					});
					
					// re-margin any stragglers
					$(".npf_inst").each(function(){
						if($(this).next().is(".npf_inst")){
							$(this).css("margin-bottom","var(--NPF-Image-Spacing)");
						}
					});
					
					// relocate AGAIN for fucking dashboard capts
					if($(this).parent().prev().is("a")){
						if($(this).parent().prev().prev().is("img")){
							$(this).insertBefore($(this).parent().prev().prev());
							$(this).css("margin-bottom",enculer)
						}
					}
					
					if($(this).prev().is("a")){
						if($(this).prev().prev().is("img")){
							$(this).insertBefore($(this).prev().prev());
							$(this).css("margin-bottom",enculer)
						}
					}
					
					var huh = $(this).parents("[post-type]");
					if(huh.attr("post-type") == "answer"){
						if($(this).hasClass("photo_origin")){
							$(this).removeClass("boooob");
						}
					}
				}
			});
		}
	},0);
	
	
	// wake up asleep npfs if the page has loaded but they haven't
	$(window).load(function(){
		$(".npf_row").each(function(){
			var nu = $(this).height();
        if (nu == "0") {
         yes();
      }
		});
		
		// relocate npfs again for old dash capts
		
		$(".photo_origin").each(function(){
			if($(this).prev().length == 0){
				if($(this).closest("blockquote").length){
					if($(this).next().is("p")){
						$(this).insertBefore($(this).parent("blockquote"));
						if($(this).prev("p").children().first().is("a.tumblr_blog")){
							$(this).insertBefore($(this).prev())
						}
					}
				}
			}
		});
		
	});
});//end ready
