### NPF images fix — v2.0

An add-on for Tumblr by [glenthemes](//glenthemes.tumblr.com) that rearranges NPF photosets to make them appear more natural (mimicking what they're meant to look like on the dashboard), as the inherent styling for those images is incompatible with most themes.

#### What is NPF?
> NPF stands for "Neue Post Format". Tumblr used to have multiple types of post formats to choose from (namingly text, photo, quote, link, chat, video, questions), but in recent years they've started to turn many of those formats into NPF only (in other words, everything becomes a text post). This means that all images uploaded via Tumblr mobile have turned into NPF images. NPF images can also refer to images between paragraphs.

#### Features:
- turns NPF photosets back to photo posts if they were meant to look like one [[example](https://meppine.tumblr.com/post/632602131876167680)]
- removes the blockquote border on photosets
- fixes image stretching and size inconsistency
- custom image spacing, without ruining the post width

#### More Info & Download:

[glenthemes.tumblr.com/post/638038350689976320](https://glenthemes.tumblr.com/post/638038350689976320)
